﻿@feature_tag
Feature: Google forms test

@basic
Scenario: Answer the questions on google form pages
	Given I open google form page
	When I answer Az, hogy megismerjem a szoftvertesztelés alapjait. to the first question
	And I answer 4 to the second question
	And I submit the first page
	When I answer Git is 4, Unit testing is 4, CI is 4 and Web UI is 5 to the third question
	And I answer 2 to the fourth question
	And I answer A tanár az elején elmagyarázza az elméletet, majd önálló feladatokat oszt ki, melyeket együtt ellenőrzünk. to the fifth question
	And I answer 8 to the sixth question
	And I submit the second page
	Then google form result should contain the success icon

@tables
Scenario Outline: Answer the questions on google form pages using 
	Given I open google form page
	When I answer the questions with the following parameters
	| FirstAnswer   | SecondAnswer   | ThirdAnswer   | FourthAnswer   | FifthAnswer   | SixthAnswer   |
	| <FirstAnswer> | <SecondAnswer> | <ThirdAnswer> | <FourthAnswer> | <FifthAnswer> | <SixthAnswer> |
	And I submit the form
	Then google form result should contain the success icon

	Examples:
	| FirstAnswer                                        | SecondAnswer | ThirdAnswer | FourthAnswer | FifthAnswer                                                                                                | SixthAnswer |
	| Az, hogy megismerjem a szoftvertesztelés alapjait. | 4            | 4 4 4 5     | 2            | A tanár az elején elmagyarázza az elméletet majd önálló feladatokat oszt ki melyeket együtt ellenörzünk.   | 8           |
	| Good                                               | 2            | 4 4 5 5     | 3            | Good                                                                                                       | 7           |