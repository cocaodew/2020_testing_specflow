﻿using specflowSudent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using WebdriverClass.Models;

namespace specflowSudent.Transformations
{
    [Binding]
    public class FormModelTransform
    {
        [StepArgumentTransformation]
        public FormModel TransformTableToSearchModel(Table table)
        {
            return table.CreateInstance<FormModel>();
        }
    }
}
