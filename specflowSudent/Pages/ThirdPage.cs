﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SpecflowSudent.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    public class ThirdPage : BasePage
    {        
        public ThirdPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        WebDriverWait Wait => new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

        public bool SuccessIconExists()
        {
            var icon = Wait.Until(d => d.FindElement(By.XPath("//i[@class='ms-Icon ms-Icon--Completed office-form-theme-primary-color thank-you-page-font-icon']")));
            if (icon != null)
                return true;
            return false;
        }
    }
}
