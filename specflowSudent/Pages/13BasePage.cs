﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace SpecflowSudent.Pages
{
    public class BasePage
    {
        protected IWebDriver Driver;

        protected WebDriverWait Wait;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
            Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
        }
    }
}
