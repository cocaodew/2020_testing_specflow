﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SpecflowSudent.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class WidgetPage : BasePage
    {
        public WidgetPage(IWebDriver webDriver, IWebElement container) : base(webDriver)
        {
            this.Container = container;
        }

        public IWebElement Container;
    }
}
