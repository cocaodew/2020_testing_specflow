﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SpecflowSudent.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class FormPage : BasePage
    {
        public FormPage(IWebDriver webDriver) : base(webDriver)
        {
            Questions = Wait.Until(d => webDriver.FindElements(By.CssSelector("div[class='__question__ office-form-question  ']")).ToList());
        }

        public List<IWebElement> Questions { get; set; }
    }
}
