﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    public class FirstPage : FormPage
    {
        public FirstPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static FirstPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://forms.office.com/Pages/ResponsePage.aspx?id=0HIbtJ9OJkyKaflJ82fJHemFnY1uiJpLseLCaxLxmwlUNUFPRjJFSU1VNDdYWlo0MFQyVTExTjBWVC4u";
            return new FirstPage(webDriver);
        }

        public TextBoxWidget GetTextBoxWidgetOfQuestion(int questionNumber)
        {
            return new TextBoxWidget(Driver, Questions[questionNumber - 1]);
        }

        public NumericRadioWidget GetRadioWidgetOfQuestion(int questionNumber)
        {
            return new NumericRadioWidget(Driver, Questions[questionNumber - 1]);
        }

        public ButtonWidget GetNextButtonWidget()
        {
            return new ButtonWidget(Driver, "Következő");
        }

        public SecondPage Next()
        {
            GetNextButtonWidget().Click();
            return new SecondPage(Driver);
        }
    }
}
