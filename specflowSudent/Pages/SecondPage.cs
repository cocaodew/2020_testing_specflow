﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    public class SecondPage : FormPage
    {
        public SecondPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public MatrixRadioWidget GetMatrixRadioWidget(int questionNumber)
        {
            return new MatrixRadioWidget(Driver, Questions[questionNumber - 1]);
        }

        public NumericRadioWidget GetNumericRadioWidget(int questionNumber)
        {
            return new NumericRadioWidget(Driver, Questions[questionNumber - 1]);
        }

        public TextBoxWidget GetTextBoxWidget(int questionNumber)
        {
            return new TextBoxWidget(Driver, Questions[questionNumber - 1]);
        }

        public BoxRadioWidget GetBoxRadioWidget(int questionumber)
        {
            return new BoxRadioWidget(Driver, Questions[questionumber - 1]);
        }

        public ButtonWidget GetSubmitButtonWidget()
        {
            return new ButtonWidget(Driver, "Küldés");
        }

        public ThirdPage Submit()
        {
            GetSubmitButtonWidget().Click();
            return new ThirdPage(Driver);
        }
    }
}
