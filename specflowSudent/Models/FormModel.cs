﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WebdriverClass.WidgetsAtClass.NumericRadioWidget;

namespace WebdriverClass.Models
{
    public class FormModel
    {
        public string FirstAnswer { get; set; }
        public int SecondAnswer { get; set; }
        public string ThirdAnswer { get; set; }
        public int FourthAnswer { get; set; }
        public string FifthAnswer { get; set; }
        public int SixthAnswer { get; set; }
    }
}
