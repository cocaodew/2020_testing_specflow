﻿using NUnit.Framework;
using OpenQA.Selenium;
using specflowSudent.Extensions;
using specflowSudent.Models;
using SpecflowSudent.Pages;
using System.Collections.Generic;
using System.Threading;
using TechTalk.SpecFlow;
using TestProject;
using WebdriverClass.Models;
using WebdriverClass.PagesAtClass;
using System.Linq;

namespace SpecflowSudent
{
    [Binding]
    class GoogleFormTestSteps : Steps
    {
        [Given(@"I open google form page")]
        public void GivenIOpenGoogleFormPage()
        {
            var firstPage = FirstPage.Navigate(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            ScenarioContext.Add(BDDTestBase.currentPage, firstPage);
        }

        [When(@"I answer (.*) to the first question")]
        public void WhenIAnswerAzHogyMegismerjemASzoftvertesztelesAlapjait_ToTheFirstQuestion(string answer)
        {
            var firstPage = new FirstPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            firstPage.GetTextBoxWidgetOfQuestion(1)
            .SetText(answer);
        }

        [When(@"I answer (.*) to the second question")]
        public void WhenIAnswerToTheSecondQuestion(int answer)
        {
            var firstPage = new FirstPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            firstPage.GetRadioWidgetOfQuestion(2)
                .SetNumericRadioOptionTo(answer);
        }

        [When(@"I submit the first page")]
        public void WhenISubmitTheFirstPage()
        {
            var firstPage = new FirstPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            firstPage.Next();
        }

        [When(@"I answer Git is (.*), Unit testing is (.*), CI is (.*) and Web UI is (.*) to the third question")]
        public void WhenIAnswerGitIsUnitTestingIsCIIsAndWebUIIsToTheThirdQuestion(int git, int unitTesting, int ci, int webUi)
        {
            var secondPage = new SecondPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            secondPage.GetMatrixRadioWidget(1)
                .SetMatrixRadioOptions(new int[] { git, unitTesting, ci, webUi });
        }

        [When(@"I answer (.*) to the fourth question")]
        public void WhenIAnswerToTheFourthQuestion(int radioIndex)
        {
            var secondPage = new SecondPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            secondPage.GetNumericRadioWidget(2)
                .SetNumericRadioOptionTo(radioIndex);
        }

        [When(@"I answer (.*) to the fifth question")]
        public void WhenIAnswerATanarAzElejenElmagyarazzaAzElmeletetMajdOnalloFeladatokatOsztKiMelyeketEgyuttEllenorzunk_ToTheFifthQuestion(string answer)
        {
            var secondPage = new SecondPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            secondPage.GetTextBoxWidget(3)
                .SetText(answer);
        }

        [When(@"I answer (.*) to the sixth question")]
        public void WhenIAnswerToTheSixthQuestion(int radioIndex)
        {
            var secondPage = new SecondPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            secondPage.GetBoxRadioWidget(4)
                .SetNumericRadioOptionTo(radioIndex);
        }

        [When(@"I submit the second page")]
        public void WhenISubmitTheSecondPage()
        {
            var secondPage = new SecondPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            var thirdPage = secondPage.Submit();
        }

        [Then(@"google form result should contain the success icon")]
        public void ThenGoogleFormResultShouldContainTheSuccessIcon()
        {
            var thirdPage = new ThirdPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            Assert.True(thirdPage.SuccessIconExists());
        }

        [When(@"I answer the questions with the following parameters")]
        public void WhenIAnswerTheQuestionsWithTheFollowingParameters(FormModel formModel)
        {
            var firstPage = new FirstPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));

            firstPage.GetTextBoxWidgetOfQuestion(1)
                .SetText(formModel.FirstAnswer);

            firstPage.GetRadioWidgetOfQuestion(2)
                .SetNumericRadioOptionTo(4);

            SecondPage secondPage = firstPage.Next();

            secondPage.GetMatrixRadioWidget(1)
                .SetMatrixRadioOptions(formModel.ThirdAnswer.Split(' ').Select(x => int.Parse(x)).ToArray());

            secondPage.GetNumericRadioWidget(2)
                .SetNumericRadioOptionTo(formModel.FourthAnswer);

            secondPage.GetTextBoxWidget(3)
                .SetText(formModel.FifthAnswer);

            secondPage.GetBoxRadioWidget(4)
                .SetNumericRadioOptionTo(formModel.SixthAnswer);
        }

        [When(@"I submit the form")]
        public void WhenISubmitTheForm()
        {
            var secondPage = new SecondPage(ScenarioContext.Get<IWebDriver>(BDDTestBase.webDriver));
            var thirdPage = secondPage.Submit();
        }
    }
}
