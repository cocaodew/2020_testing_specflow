﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SpecflowSudent.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class ButtonWidget : BasePage
    {
        public ButtonWidget(IWebDriver webDriver, string title) : base(webDriver)
        {
            this.Title = title;
        }

        public string Title;
        public IWebElement Button => Wait.Until(d => Driver.FindElement(By.CssSelector($"button[title='{Title}']")));

        public void Click()
        {
            Button.Click();
        }
    }
}
