﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class TextBoxWidget : WidgetPage
    {
        public TextBoxWidget(IWebDriver webDriver, IWebElement container) : base(webDriver, container)
        {
        }

        public IWebElement TextInput => Wait.Until(d => Container.FindElement(By.CssSelector("input[class='office-form-question-textbox office-form-textfield-input form-control office-form-theme-focus-border border-no-radius']")));

        public void SetText(string text)
        {
            TextInput.SendKeys(text);
        }
    }
}
