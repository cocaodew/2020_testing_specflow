﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class NumericRadioWidget : WidgetPage
    {
        public NumericRadioWidget(IWebDriver webDriver, IWebElement container) : base(webDriver, container)
        {
        }

        List<IWebElement> Options => Wait.Until(d => Container.FindElements(By.CssSelector("input[type='radio']")).ToList());

        public void SetNumericRadioOptionTo(int index)
        {
            Options[index - 1].Click();
        }
    }
}
