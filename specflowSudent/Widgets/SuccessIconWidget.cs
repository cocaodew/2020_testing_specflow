﻿using OpenQA.Selenium;
using SpecflowSudent.Pages;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class SuccessIconWidget : BasePage
    {
        public SuccessIconWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public IWebElement Icon => Driver.FindElement(By.CssSelector("i[class='ms-Icon ms-Icon--Completed office-form-theme-primary-color thank-you-page-font-icon']"));
    }
}