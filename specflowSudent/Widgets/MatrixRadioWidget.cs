﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class MatrixRadioWidget : WidgetPage
    {

        public MatrixRadioWidget(IWebDriver webDriver, IWebElement container) : base(webDriver, container)
        {
        }

        public List<IWebElement> Rows => Wait.Until(d => Container.FindElements(By.CssSelector("div[class='office-form-matrix-row']")).ToList());

        public void SetMatrixRadioOptions(int[] indices)
        {
            for (int i = 0; i < indices.Length; i++)
            {
                var cells = Wait.Until(d => Rows[i].FindElements(By.CssSelector("input[type='radio']")).ToList());
                cells[indices[i] - 1].Click();
            }
        }
        
    }
}
