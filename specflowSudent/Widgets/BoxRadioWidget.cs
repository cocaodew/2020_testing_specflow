﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class BoxRadioWidget : WidgetPage
    {
        public BoxRadioWidget(IWebDriver webDriver, IWebElement container) : base(webDriver, container)
        {
        }

        public List<IWebElement> Options => Wait.Until(d => Container.FindElements(By.CssSelector("label[class='answer-label touch office-form-theme-button-foreground  ']")).ToList());

        public void SetNumericRadioOptionTo(int index)
        {
            Options[index].Click();
        }
    }
}
